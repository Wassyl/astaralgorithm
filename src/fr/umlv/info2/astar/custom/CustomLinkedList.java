package fr.umlv.info2.astar.custom;

import java.util.HashSet;
import java.util.StringJoiner;

public class CustomLinkedList implements Border {

    /**
     * An Entry represents a cell of the list.
     * Contains both the vertex and its value and points to the next Entry.
     */
    private static class Entry{
        private final int vertex;
        private final int value;
        private Entry next;

        private Entry(int vertex, int value, Entry next){
            this.vertex = vertex;
            this.value = value;
            this.next = next;
        }

        private int getVertex() {
            return vertex;
        }

        private int getValue() { return value; }
    }

    private Entry first;
    private int size;
    private final HashSet<Integer> hashset = new HashSet<>();

    public CustomLinkedList() {
        this.first = null;
    }

    /**
     * Adding to the list with O(n) complexity.
     * The insertion will keep the list sorted.
     */
    public void add(int vertex, int value) {
        switch ( size ){
            // List is empty, 'first' becomes the new node
            case 0:
                first = new Entry(vertex, value, null);
                break;
            // List only has 'first' as a node, make sure the new becomes 'first' or 'first.next'
            case 1:
                if( value <= first.getValue()  ){
                    var newEntry = new Entry(vertex, value, first);
                    first.next = null;
                    first = newEntry;
                } else {
                    first.next = new Entry(vertex, value, null);
                }
                break;
            // Goes through the list until it finds a higher value
            default:
                Entry current = first;
                for( ; current.next != null && current.value < value ; current = current.next) {
                    // nothing to do here
                }
                if( current.next == null ){
                    current.next = new Entry(vertex, value, null);
                } else {
                    var oldNext = current.next;
                    current.next = new Entry(vertex, value, oldNext);
                }
        }
        size++;
        hashset.add(vertex);
    }

    /**
     * Extracts the first node with O(1) complexity.
     * @return the vertex associated
     */
    public int extractMin() {
        if( isEmpty() ) {
            throw new IllegalStateException("Heap is empty");
        }
        // Recover the vertex
        var vertex = first.getVertex();
        // The second entry becomes the first
        first = first.next;
        size--;
        hashset.remove(vertex);
        return vertex;
    }

    /**
     * Removes the node and reinserts it in order to keep the list sorted.
     * Both operations with O(n) complexity.
     * Uses the add() function.
     */
    public void updateVertex(int vertex, int oldValue, int newValue) {
        if( isEmpty() ) {
            throw new IllegalStateException("Heap is empty");
        }
        // Removes the vertex
        removeVertex(vertex);
        size--;
        hashset.remove(vertex);
        // Then re-adds it
        add(vertex, newValue);
    }

    /**
     * Parses the list and removes the vertex with O(n) complexity.
     */
    private void removeVertex(int vertex) {
        switch ( size ){
            // Only the first matches
            case 1:
                first = null;
                break;
            case 2:
                if( first.vertex == vertex ){
                    first = first.next;
                } else {
                    first.next = null;
                }
                break;
            default:
                if( first.vertex == vertex ){
                    first = first.next;
                    break;
                }
                var current = first;
                for( ; current.next != null &&  current.next.vertex != vertex ; current = current.next) {
                    // nothing to do here
                }
                if( current.next != null ) {
                    var oldNext = current.next.next;
                    current.next.next = null;
                    current.next = oldNext;
                    break;
                }
        }
    }
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Uses a hashset to do the work.
     */
    public boolean contains(int y) {
        return hashset.contains(y);
    }

    @Override
    public String toString() {
        var joiner = new StringJoiner(", ","[ "," ]");
        for(var current = first ; current != null ; current = current.next) {
            joiner.add(Integer.toString(current.getVertex()));
        }
        return joiner.toString();
    }
}
