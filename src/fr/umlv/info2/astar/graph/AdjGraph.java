package fr.umlv.info2.astar.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.function.Consumer;

public class AdjGraph implements Graph {

	private final ArrayList<LinkedList<Edge>> adj;
	private final int n;   // number of vertices
	
	public AdjGraph(int n) {
		if( n <= 0 ) {
			throw new IllegalArgumentException("n must be positive");
		}
		this.n = n;
		this.adj = new ArrayList<>(n);
		for(var i = 0; i != n;i++) {
			adj.add( new LinkedList<>());
		}
	}
	
	@Override
	public int numberOfEdges() {
		var counter = 0;
		for( var ll : adj) {
			counter += ll.size();
		}
		return counter;
	}

	@Override
	public int numberOfVertices() {
		return n;
	}

	@Override
	public void addEdge(int i, int j, int value) {
		if( value == 0 ) {
			return;
		}
		Objects.checkIndex(i, n);
		Objects.checkIndex(j, n);
		
		var ll = adj.get(i);
		ll.add(new Edge(i,j,value));
	}

	@Override
	public boolean isEdge(int i, int j) {
		Objects.checkIndex(i, n);
		Objects.checkIndex(j, n);
		var ll = adj.get(i);
		for( var e : ll ) {
			if(e.getEnd() == j) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int getWeight(int i, int j) {
		Objects.checkIndex(i, n);
		Objects.checkIndex(j, n);
		var ll = adj.get(i);
		for( var e : ll ) {
			if(e.getEnd() == j) {
				return e.getValue();
			}
		}
		return 0;
	}

	@Override
	public Iterator<Edge> edgeIterator(int i) {
		Objects.checkIndex(i, n);
		var ll = adj.get(i);
		return ll.iterator();
	}

	@Override
	public void forEachEdge(int i, Consumer<Edge> consumer) {
		for(var it = this.edgeIterator(i); it.hasNext();) {
			consumer.accept(it.next());
		}
	}

	@Override
	public String toGraphviz() {
		var sb = new StringBuilder("digraph G {"+System.lineSeparator());
		for(var i = 0; i != n ; i++) {
			sb.append("\t"+i + ";"+System.lineSeparator());
			forEachEdge(i,(e) -> sb.append("\t"+e.getStart() +" -> "+ e.getEnd()+" [ label=\""+ e.getValue() +"\"] ;"+System.lineSeparator()));
		}
		return sb.append("}").toString();
	}

}
