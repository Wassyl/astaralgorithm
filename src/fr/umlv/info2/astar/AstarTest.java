package fr.umlv.info2.astar;

import fr.umlv.info2.astar.graph.AdjGraph;
import fr.umlv.info2.astar.graph.Graph;
import fr.umlv.info2.astar.graph.Graphs;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AstarTest {

    public static final Astar ASTAR_EXAMPLE;
    public static final Astar ASTAR_NEW_YORK;
    public static final Astar ASTAR_WESTERN;
    public static final Astar ASTAR_USA;

    public static final Graph GRAPH_EXAMPLE;
    public static final Graph GRAPH_NEW_YORK;

    public static final int DEFAULT_SOURCE = 0;
    public static final int DESTINATION_EXAMPLE = 6;
    public static final int DESTINATION_NEW_YORK = 264345;
    public static final int DESTINATION_WESTERN = 6262103;
    public static final int DESTINATION_USA = 264345;

    static {
        try {
            ASTAR_EXAMPLE = Graphs.makeAstarFromFiles("example", AdjGraph::new);
            ASTAR_NEW_YORK = Graphs.makeAstarFromFiles("USA-road-d.NY", AdjGraph::new);
            ASTAR_WESTERN = Graphs.makeAstarFromFiles("USA-road-d.w", AdjGraph::new);

            //ASTAR_USA = Graphs.makeAstarFromFiles("USA-road-d.USA", AdjGraph::new);
            ASTAR_USA = ASTAR_EXAMPLE; // Too big !!

            GRAPH_EXAMPLE = Graphs.makeGraphFromGraphFile(Path.of("example.gr"), AdjGraph::new);
            GRAPH_NEW_YORK = Graphs.makeGraphFromGraphFile(Path.of("USA-road-d.NY.gr"), AdjGraph::new);
        } catch (IOException e) {
            throw new AssertionError("Couldn't find the files at the root of the project !");
        }
    }

    /* TEST READING */

    @Test
    public void exampleRead() {
        assertEquals(139, GRAPH_EXAMPLE.getWeight(0,3));
        assertEquals(88, GRAPH_EXAMPLE.getWeight(0,5));
        assertEquals(122, GRAPH_EXAMPLE.getWeight(5,4));
        assertEquals(133, GRAPH_EXAMPLE.getWeight(6,0));
    }

    @Test
    public void newYorkRead() {
        assertEquals(1285, GRAPH_NEW_YORK.getWeight(0,1));
        assertEquals(1285, GRAPH_NEW_YORK.getWeight(1,0));
        assertEquals(253, GRAPH_NEW_YORK.getWeight(2,3));
        assertEquals(1238, GRAPH_NEW_YORK.getWeight(4,5));
    }

    /* EXAMPLE SUBJECT FILE */

    @Test
    public void exampleDijkstra() {
        var dijkstra = Graphs.dijkstra(GRAPH_EXAMPLE, DEFAULT_SOURCE);
        System.out.println("Dijkstra :");
        dijkstra.printShortestPathTo(6);
    }

    @Test
    public void exampleDijkstraWithHeap() {
        var dijkstra = Graphs.dijkstraWithHeap(GRAPH_EXAMPLE, DEFAULT_SOURCE);
        System.out.println("Dijkstra-BinaryHeap:");
        dijkstra.printShortestPathTo(6);
    }


    @Test
    public void exampleLinkedList() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_EXAMPLE.computeWithLinkedList(DEFAULT_SOURCE,DESTINATION_EXAMPLE);
            System.out.println("Astar - computeWithLinkedList() :");
            shortestPathFromOneVertex.printShortestPathTo(6);
        });
    }

    @Test
    public void exampleCustomLinkedList() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_EXAMPLE.computeWithCustomLinkedList(DEFAULT_SOURCE,DESTINATION_EXAMPLE);
            System.out.println("Astar - computeWithCustomLinkedList() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_EXAMPLE);
        });
    }

    @Test
    public void exampleCustomHeap() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_EXAMPLE.computeWithCustomBinaryHeap(DEFAULT_SOURCE,DESTINATION_EXAMPLE);
            System.out.println("Astar - computeWithCustomBinaryHeap() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_EXAMPLE);
        });
    }

    /* NEW YORK */

    // TODO ADD THIS TEST
    //@Test
    public void newYorkDijkstra() {
        var dijkstra = Graphs.dijkstra(GRAPH_NEW_YORK, DEFAULT_SOURCE);
        System.out.println("Dijkstra :");
        dijkstra.printShortestPathTo(DESTINATION_NEW_YORK);
    }

    @Test
    public void newYorkCustomLinkedList() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_NEW_YORK.computeWithCustomLinkedList(DEFAULT_SOURCE,DESTINATION_NEW_YORK);
            System.out.println("Astar - computeWithCustomLinkedList() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_NEW_YORK );
        });
    }

    @Test
    public void newYorkCustomHeap() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_NEW_YORK.computeWithCustomBinaryHeap(DEFAULT_SOURCE,DESTINATION_NEW_YORK);
            System.out.println("Astar - computeWithCustomBinaryHeap() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_NEW_YORK );
        });
    }

    /* WESTERN */

    @Test
    public void westernCustomLinkedList() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_WESTERN.computeWithCustomLinkedList(DEFAULT_SOURCE,DESTINATION_WESTERN);
            System.out.println("Astar - computeWithCustomLinkedList() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_WESTERN );
        });
    }

    @Test
    public void westernCustomHeap() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_WESTERN.computeWithCustomBinaryHeap(DEFAULT_SOURCE,DESTINATION_WESTERN);
            System.out.println("Astar - computeWithCustomBinaryHeap() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_WESTERN );
        });
    }

    /* USA */

    @Test
    public void usaCustomLinkedList() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_USA.computeWithCustomLinkedList(DEFAULT_SOURCE,DESTINATION_USA);
            System.out.println("Astar - computeWithCustomLinkedList() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_USA );
        });
    }

    @Test
    public void usaCustomHeap() {
        assertDoesNotThrow(() -> {
            var shortestPathFromOneVertex = ASTAR_USA.computeWithCustomBinaryHeap(DEFAULT_SOURCE,DESTINATION_USA);
            System.out.println("Astar - computeWithCustomBinaryHeap() :");
            shortestPathFromOneVertex.printShortestPathTo(DESTINATION_USA );
        });
    }
}
