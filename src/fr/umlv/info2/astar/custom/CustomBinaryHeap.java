package fr.umlv.info2.astar.custom;

import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;

public class CustomBinaryHeap implements Border {

    /**
     * A cell of the array is a Node.
     * Contains the vertex and its value.
     */
    private static class Node{
        private final int vertex;
        private int value;

        private Node(int vertex, int value){
            this.vertex = vertex;
            this.value = value;
        }

        private int getVertex() {
            return vertex;
        }

        private int getValue() { return value; }

        private void setValue( int newValue) { value = newValue; }
    }

    // Default size for the array
    private final static int DEFAULT_SIZE = 32;
    // Array storing the nodes
    private Node[] heap = new Node[DEFAULT_SIZE];

    // Used to inform which vertex is in the array currently.
    private final boolean[] contains;

    private int size;

    public CustomBinaryHeap(int n) {
        this.contains = new boolean[n];
    }
    /**
     * Adds a vertex to the heap at the end of the array.
     * Will compute swaps with the parent if needed to leave the heap sorted.
     */
    public void add(int vertex, int value) {
        if( size == heap.length ){
            grow();
        }
        //hashset.add(vertex);
        contains[vertex] = true;
        heap[size] = new Node(vertex, value);
        size++;

        var current = size - 1;
        while ( heap[current].getValue() < heap[parentOf(current)].getValue() ) {
            swap(current, parentOf(current));
            current = parentOf(current);
        }
        //updateHeap(0);
    }

    /**
     * Extracts the minimum of the heap with O(1) complexity.
     * Recomputes a update in order to leave the heap sorted.
     */
    public int extractMin() {
        if( isEmpty() ) {
            throw new IllegalStateException("Heap is empty");
        }
        var root = heap[0];
        heap[0] = heap[size - 1]; // In order to remove the head
        size--;
        //hashset.remove(root.getVertex());
        contains[root.getVertex()] = false;
        updateHeap(0);
        return root.getVertex();
    }

    /**
     * Parses the heap with O(n) complexity and changes its value.
     * Re-updates the heap after the modification.
     */
    public void updateVertex(int vertex, int oldValue, int newValue) {
        if ( isEmpty() ){
            throw new IllegalStateException("Heap is empty");
        }
        for (var node : heap) {
            if (node.getVertex() == vertex) {
                node.setValue(newValue);
                updateHeap(0);
                break;
            }
        }
    }

    public boolean isEmpty() { return size == 0; }

    /**
     * Uses another boolean array to avoid a O(n) parse.
     * The vertex's number points its cell.
     */
    public boolean contains(int y) {
        Objects.checkIndex(y, contains.length);
        return contains[y];
    }

    /**
     * Makes sure the heap is sorted with a O(log(n)) parsing.
     * Swaps different positions during the parse, between parent and childs.
     */
    private void updateHeap(int pos) {
        // Must not be computed on leaves.
        if ( !isLeaf(pos) ) {
            if (heap[pos].getValue() > heap[leftChild(pos)].getValue() || heap[pos].getValue() > heap[rightChild(pos)].getValue() ) {
                if (heap[leftChild(pos)].getValue() < heap[rightChild(pos)].getValue() ) {
                    swap(pos, leftChild(pos));
                    updateHeap(leftChild(pos));
                } else {
                    swap(pos, rightChild(pos));
                    updateHeap(rightChild(pos));
                }
            }
        }
    }

    /**
     * Returns true if the vertex is a leaf.
     */
    private boolean isLeaf(int pos) {
        return pos >= (size >>> 1) && pos <= size;
    }

    private int parentOf(int pos) { return pos >>> 1; }
    private int leftChild(int pos){ return pos*2; }
    private int rightChild(int pos){ return (pos*2)+1; }

    /**
     * Swaps two vertices.
     */
    private void swap(int first, int second) {
        var tmpNode = heap[first];
        heap[first] = heap[second];
        heap[second] = tmpNode;
    }

    /**
     * Doubles the size of the heap.
     */
    private void grow() { heap = Arrays.copyOf(heap, heap.length*2); }

    @Override
    public String toString() {
        var joiner = new StringJoiner(", ","[ "," ]");
        Arrays.stream(heap).filter(Objects::nonNull).forEach(e -> joiner.add(Integer.toString(e.getVertex())));
        return joiner.toString();
    }
}
