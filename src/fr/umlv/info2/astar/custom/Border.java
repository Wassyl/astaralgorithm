package fr.umlv.info2.astar.custom;

public interface Border {

    /**
     * Add a
     * @param vertex
     * @param value
     */
    void add(int vertex, int value);

    /**
     * The extraction must be in O(1).
     * @return vertex who had the minimum value
     */
    int extractMin();

    /**
     * The parse may be in O(n) to find the vertex.
     * Then must leave the object still sorted.
     * @param vertex
     * @param oldValue
     * @param newValue
     */
    void updateVertex(int vertex, int oldValue, int newValue);

    /**
     * Returns a boolean telling if the border instance is empty.
     * @return
     */
    boolean isEmpty();

    /**
     * HashSet are used to avoid this and store the vertices to avoid a O(n) parsing.
     * @param vertex
     * @return
     */
    boolean contains(int vertex);
}
