# AstarAlgorithm

Implementation of Astar (A*) Algorithm 

ESIPE / 2020-2021

Members :
- Ana Margarida Albuquerque
- Margarida Noronha
- Oummou AMPOUMET 
- Wassyl EL HACHIMI 

# How to run the program :
```
java Astar file # Source becomes 1 and Destination becomes V (number of vertices)
java Astar file source destination
```
You also can run **AstarTest** class to run a few tests for the example given in the subject, New York, Western Usa or USA.

# Project structure 

```
____
    |___ custom - Contains implementations for a queue usage ( 'border' ) 
    |
    |___ graph - Contains implementations to simulate a graph usage
    |
    |___ shortestPath - Classes used to print a shortest path after a call on the A* algorithm
    |
    |_ Astar.java
```

The **Border** interface provides 5 methods:
```Java

public interface Border {

    /**
     * Add a vertice to the queue. Leaves the queue sorted.
     * @param vertex
     * @param value
     */
    void add(int vertex, int value);

    /**
     * The extraction must be in O(1).
     * @return vertex who had the minimum value
     */
    int extractMin();

    /**
     * The parse may be in O(n) to find the vertex.
     * Then must leave the object still sorted.
     * @param vertex
     * @param oldValue
     * @param newValue
     */
    void updateVertex(int vertex, int oldValue, int newValue);

    /**
     * Returns a boolean telling if the border instance is empty.
     * @return
     */
    boolean isEmpty();

    /**
     * Arrays with V vertices/cells are used to keep the information and avoid O(n) parsing.
     * @param vertex
     * @return
     */
    boolean contains(int vertex);
}
```
2 Custom implementations were made:

## Linked List 

- Main difference is that this custom linked list, when it adds a new vertex, will keep the queue still sorted.
- Also, it updates a vertex. It removes the vertex, changes its weight, and reuses the add() 
function, because the weight update will change the behaviour because it will not be sorted anymore.
- Add, update have O(n) complexities.
- Uses a hashset for contains() method. We could use a array for V vertices to have the same result (no time to correct the code). Else we could just parse the list to find the vertice, but this extends the computing time when running the algorithm.

## Binary Heap

- Uses a array to simulate the heap.
- Add a vertex has a O(log(n)) complexity because it all depends on the size of the tree.
- Updating a vertex also has a O(log(n)) complexity.
- Contains uses a boolean array so it is done with O(1) complexity. O(n) if we parse the array to find the vertex.

# Results with tests

![AstarTest](https://gitlab.com/Wassyl/astaralgorithm/-/raw/master/ASTAR_TEST.PNG)

We can see that with a heap usage, it goes much faster, from 10 seconds to 4 seconds with the Western map.

The examples files are given on the repository.

If you run the test and you don't have the files, you will have an **AssertionError**.
# Tests already written

Subject: 
The A* algorithm is designed to provide the fastest search for the quickest path between two nodes. It requires geographical information like coordinates for each nodes and distances between each of them.

For this project, we will be using northern american data : 
```
(*.gr) - Edges between vertices 
(*.co) - Geographical coordinates

```
The edges weight in the (*.gr) files are miles, not kilometers.

