package fr.umlv.info2.astar.graph;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

public class MatGraph implements Graph {
	private final int[][] mat;
	private final int n; // number of vertices
	
	public MatGraph(int n) {
		if( n <= 0 ) {
			throw new IllegalArgumentException("n must be positive");
		}
		this.n = n;
		var tmpArray = new int[n][n];
		for(var i = 0; i != n ; i++) {
			for(var j=0; j != n; j++) {
				tmpArray[i][j] = 0;
			}
		}
		this.mat = tmpArray;
	}
	
	@Override
	public int numberOfEdges() {
		var counter = 0;
		for(var i = 0; i != n ; i++) {
			for(var j=0; j != n; j++) {
				if(mat[i][j] != 0) {
					counter++;
				}
			}
		}
		return counter;
	}
	
	@Override
	public int numberOfVertices() {
		return n;
	}
	
	@Override
	public void addEdge(int i, int j, int value) {
		if( value == 0 ) {
			return;
		}
		Objects.checkIndex(i, n);
		Objects.checkIndex(j, n);
		mat[i][j] = value;
	}
	
	@Override
	public boolean isEdge(int i, int j) {
		Objects.checkIndex(i, n);
		Objects.checkIndex(j, n);
		return mat[i][j] != 0;
	}
	
	@Override
	public int getWeight(int i, int j) {
		Objects.checkIndex(i, n);
		Objects.checkIndex(j, n);
		return mat[i][j];
	}
	
	@Override
	public Iterator<Edge> edgeIterator(int i) {
		Objects.checkIndex(i, n);
		return new Iterator<Edge>() {
			private int current = computeNext(0);
			
			private int computeNext(int start) {
				for( var e = start; e < n; e++) {
					if( mat[i][e] != 0 ) {
						return e;
					}
				}
				return -1;
			}
			
			@Override
			public boolean hasNext() {
				return current != -1;
			}

			@Override
			public Edge next() {
				if( !hasNext() ) {
					throw new NoSuchElementException();
				}
				var tmp = current;
				current = computeNext(current + 1);
				return new Edge(i,tmp, mat[i][tmp]);
			}
			
		};
	}
	
	@Override
	public void forEachEdge(int i, Consumer<Edge> consumer) {
		for(var it = this.edgeIterator(i); it.hasNext();) {
			consumer.accept(it.next());
		}
	}

	@Override
	public String toGraphviz() {
		var sb = new StringBuilder("digraph G {"+System.lineSeparator());
		for(var i = 0; i != n ; i++) {
			sb.append("\t"+i + ";"+System.lineSeparator());
			forEachEdge(i,(e) -> sb.append("\t"+e.getStart() +" -> "+ e.getEnd()+" [ label=\""+ e.getValue() +"\"] ;"+System.lineSeparator()));
		}
		return sb.append("}").toString();
	}
}
