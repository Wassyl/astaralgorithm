package fr.umlv.info2.astar;

import fr.umlv.info2.astar.custom.Border;
import fr.umlv.info2.astar.custom.CustomBinaryHeap;
import fr.umlv.info2.astar.custom.CustomLinkedList;
import fr.umlv.info2.astar.graph.AdjGraph;
import fr.umlv.info2.astar.graph.Graph;
import fr.umlv.info2.astar.graph.Graphs;
import fr.umlv.info2.astar.shortestPath.ShortestPathFromOneVertex;

import java.io.IOException;
import java.util.*;
import java.util.function.Supplier;

public class Astar {

    // Reasonably high value used to make comparisons in the A* algorithm
    private final static int MAX_VALUE = 1_000_000;
    // Graph is a AdjGraph instance storing all nodes read in the specified file (.gr)
    private final Graph graph;
    // Works like int[n][2], with every cell corresponds to a pair of coordinates. All coordinates read in the specified file (*.co)
    private final int[][] coordinates;

    /**
     * Constructor specified if the user wants to compute two particular nodes.
     * @param graph Graph built with AdjGraph implementation
     * @param coords int[n][2] containing every coordinates for each vertex
     */
    public Astar(Graph graph, int[][] coords) {
        this.graph = graph;
        this.coordinates = coords;
    }

    /**
     * Returns the total number of vertices.
     */
    public int getSizeVertices() { return graph.numberOfVertices(); }

    /**
     * Used to calculate a single cell of h[] with the coordinates in the file given.
     * This computes the distance between a node and the target geographically.
     */
    private double distance( int node, int target ) {
        return Math.sqrt( Math.pow(coordinates[target][0] - coordinates[node][0], 2) + Math.pow(coordinates[target][1] - coordinates[node][1],2) );
    }

    /**
     * Computes the A* algorithm. The user may specify a Border implementation which are custom implementation
     * for A* algorithm in the 'custom' package.
     * @param source starting vertex
     * @param target ending vertex
     * @param factory Supplier specifying the implementation the function will use for extracting, adding or updating
     * @return ShortestPathFromOneVertex object containing the results
     */
    public ShortestPathFromOneVertex compute(int source, int target, Supplier<Border> factory) {
        var nbVertices = graph.numberOfVertices();
        var f = new int[nbVertices];
        var g = new int[nbVertices];
        var h = new int[nbVertices];
        var pi = new int[nbVertices];
        pi[source] = -1;

        Arrays.fill(g, MAX_VALUE);
        g[source] = 0;

        // Compute all distances with coordinates
        for(var i = 0; i < nbVertices; i++) {
            h[i] = (int) distance(i, target);
        }

        // Number of loops in the main loop
        var steps = 0;
        var border = factory.get();
        var computed = new HashSet<Integer>();

        border.add(source, f[source]);
        computed.add(source);
        while( !border.isEmpty() ) {
            // Number of loops in the main loop
            steps++;
            // removes head
            var vertex = border.extractMin();
            if( vertex == target ){
                return new ShortestPathFromOneVertex(source, f, pi, steps);
            }

            // For all successors y of x in the graph
            graph.forEachEdge( vertex, e -> {
                var x = e.getStart();
                var y = e.getEnd();
                if( computed.contains(y) ){
                    if( g[y] > g[x] + e.getValue() ) {
                        g[y] = g[x] + e.getValue();
                        var oldValue = f[y];
                        f[y] = g[y] + h[y];
                        pi[y] = x;
                        if( !border.contains(y) ){
                            border.add(y, f[y]);
                        } else {
                            border.updateVertex(y, oldValue, f[y]);
                        }
                    }
                } else {
                    g[y] = g[x] + e.getValue();
                    f[y] = g[y] + h[y];
                    pi[y] = x;
                    border.add(y, f[y]);
                    computed.add(y);
                }
            });
        }
        throw new IllegalStateException("Target is unreachable !");
    }

    public ShortestPathFromOneVertex computeWithCustomLinkedList(int source, int target) {
        return compute(source, target, CustomLinkedList::new);
    }

    public ShortestPathFromOneVertex computeWithCustomBinaryHeap(int source, int target) {
        return compute(source, target, () -> new CustomBinaryHeap(graph.numberOfVertices()));
    }

    public ShortestPathFromOneVertex computeWithLinkedList(int source, int target) { // TODO mistake
        var nbVertices = graph.numberOfVertices();
        var f = new int[nbVertices];
        var g = new int[nbVertices];
        var h = new int[nbVertices];
        var pi = new int[nbVertices];
        pi[source] = -1;

        Arrays.fill(f, MAX_VALUE);
        f[source] = 0;
        Arrays.fill(g, MAX_VALUE);
        g[source] = 0;

        // compute h
        for(var i = 0; i < nbVertices; i++) {
            h[i] = (int) distance(i, target);
        }

        var steps = 0;
        var border = new LinkedList<Integer>();
        var computed = new ArrayList<Integer>();

        border.add(source);
        computed.add(source);

        while( !border.isEmpty() ) {
            //System.out.println("Border : " + border);
            // Compute smallest value in border with the smallest value in f[]
            var vertex = border.stream().min(Comparator.comparingInt(x -> f[x])).get();
            if( vertex == target ){
                return new ShortestPathFromOneVertex(source, f, pi, steps);
            }
            border.remove(vertex);

            steps++;
            // For all successors y of x in the graph
            graph.forEachEdge(vertex, e -> {
                var x = e.getStart();
                var y = e.getEnd();
                if( computed.contains(y) ){
                    if( g[y] > g[x] + e.getValue() ) {
                        g[y] = g[x] + e.getValue();
                        f[y] = g[y] + h[y];
                        pi[y] = x;
                        if(!border.contains(y)){
                            border.add(y);
                        }
                    }
                } else {
                    g[y] = g[x] + e.getValue();
                    f[y] = g[y] + h[y];
                    pi[y] = x;
                    border.add(y);
                    computed.add(y);
                }
            });
        }
        throw new IllegalStateException("Target is unreachable !");
    }

    /**
     * Build a A* object, computes the shortest path and prints it.
     * Two ways to test the program :
     * java Astar file
     * java Astar file source destination
     *
     * source and destination are integers
     */
    public static void main(String[] args) throws IOException {
        Astar astar;
        ShortestPathFromOneVertex shortestPathFromOneVertex;

        switch (args.length) {
            case 1 -> {
                astar = Graphs.makeAstarFromFiles(args[0], AdjGraph::new);
                var n = astar.getSizeVertices() - 1;
                shortestPathFromOneVertex = astar.computeWithCustomBinaryHeap(0, n);
                shortestPathFromOneVertex.printShortestPathTo(n);
            }
            case 3 -> {
                var source = Integer.parseInt(args[1]);
                var destination = Integer.parseInt(args[2]);
                astar = Graphs.makeAstarFromFiles(args[0], AdjGraph::new);
                shortestPathFromOneVertex = astar.computeWithCustomBinaryHeap(source, destination);
                shortestPathFromOneVertex.printShortestPathTo(destination);
            }
            default -> usage();
        }
    }

    private static void usage(){
        System.out.println("java Astar file");
        System.out.println("java Astar file source destination");
    }
}