package fr.umlv.info2.astar.shortestPath;

import java.util.Arrays;

public class ShortestPathFromOneVertex {
	private final int source;
	private final int[] d;
	private final int[] pi;
	private final int steps;

	public ShortestPathFromOneVertex(int vertex, int[] d, int[] pi, int steps) {
		this.source = vertex;
		this.d = d;
		this.pi = pi;
		this.steps = steps;
	}

	@Override
	public String toString() {
		return source + " " + Arrays.toString(d) + " " + Arrays.toString(pi);
	}

	/**
	 * Prints the shortest path, its length and the number of steps in the main loop of A*.
	 */
	public void printShortestPathTo(int destination) {
		var sb = new StringBuilder(steps + " steps\n");

		sb.append("Path of length "+ d[destination] +" from "+source+" to "+destination+" = ");
		sb.append(getPathTo(destination));

		System.out.println(sb.toString());
	}

	/**
	 * Returns the shortest path between the source and destination.
	 */
	private String getPathTo(int destination) {
		var sb = new StringBuilder();
		sb.append(destination);

		var current = destination;
		while( pi[current] != -1 && current != pi[current] ) {
			sb.append(" >-- ").append(pi[current]);
			current = pi[current];
		}
		return sb.reverse().toString();
	}
}
