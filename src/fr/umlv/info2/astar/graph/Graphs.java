package fr.umlv.info2.astar.graph;

import fr.umlv.info2.astar.Astar;
import fr.umlv.info2.astar.custom.CustomBinaryHeap;
import fr.umlv.info2.astar.shortestPath.ShortestPathFromOneVertex;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class Graphs {

	private final static Integer MAX_VALUE = 1_000_000;
	private final static float TO_KM = 1.6f;

	/**
	 * Reads a pair of file *.gr and *.co to build an A* object.
	 * The source and destination will be 0 and n-1 respectfully.
	 */
	public static Astar makeAstarFromFiles(String fileName, IntFunction<Graph> factory) throws IOException {
		var graph = makeGraphFromGraphFile(Path.of(fileName+".gr"), factory);
		var coords = loadCoordinates(Path.of(fileName+".co"), graph.numberOfVertices());
		return new Astar(graph, coords);
	}

	/**
	 * Reads a *.gr file specifying all nodes and its edges with distances (in miles).
	 * Used to build a A* object.
	 */
	public static Graph makeGraphFromGraphFile(Path path, IntFunction<Graph> factory) throws IOException {
		// Used to stream with the lines contained in the file
		var reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);

		// Line : p sp numberOfNodes numberOfEdges
		// p sp 264346 733846
		var infoLine = reader.lines()
				.filter( line -> line.startsWith("p"))
				.findFirst()
				.get()
				.split(" ");

		// Reads this integer
		//        |
		// p sp 264346 733846
		var n = Integer.parseInt(infoLine[2]);
		var graph = factory.apply(n);
		reader.lines()
				.filter( line -> line.startsWith("a"))
				.forEach( line -> {
					// Example : ["a", "1", "2", "803"]
					var tmp = line.split(" ");
					// We remove 1 to each node in order to go from 0 to n-1, not 1 to n.
					// Example : [1, 2, 803*1,6f]
 					graph.addEdge(Integer.parseInt(tmp[1]) -1 , Integer.parseInt(tmp[2]) - 1, Math.round(Integer.parseInt(tmp[3]) * TO_KM)); // Convert Edge value to kilometers !
				});
		return graph;
	}

	/**
	 * Reads a *.co file specifying all nodes and its geographical coordinates.
	 * Used to build a A* object.
	 */
	private static int[][] loadCoordinates(Path path, int size ) throws IOException {
		// Used to stream with the lines contained in the file
		var reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);

		// Every cell corresponds to a pair of coordinates
		var coordinates = new int[size][2];
		reader.lines()
				.filter( line -> line.startsWith("a"))
				.forEach( line -> {
					// Example : ["v", "1", "-73530767", "41085396"]
					var tmp = line.split(" ");
					var nodeIndex = Integer.parseInt(tmp[0]) - 1 ;
					coordinates[nodeIndex][0] = Integer.parseInt(tmp[2]);
					coordinates[nodeIndex][1] = Integer.parseInt(tmp[3]);
				});
		return coordinates;
	}

	public static ShortestPathFromOneVertex dijkstra(Graph g, int source) {
		var nbVertices = g.numberOfVertices();
		var d = new int[nbVertices];
		var pi = new int[nbVertices];
		pi[source] = -1;
		
		Arrays.fill(d, MAX_VALUE);
		d[source] = 0;

		var steps = 0;
		var todo = new ArrayList<Integer>();
		todo.add(source);
		
		while( !todo.isEmpty() ) {
			steps++;
			var current = todo.remove(0);
			var list = new ArrayList<Map.Entry<Integer, Integer>>();
			g.forEachEdge(current, e -> {
				if( d[e.getEnd()] == MAX_VALUE ) {
					list.add(Map.entry(e.getEnd(), e.getValue()));
				}
			});

			list.sort(Comparator.comparingInt(Map.Entry::getValue));
			todo.addAll(list.stream().map(Map.Entry::getKey).collect(Collectors.toList()));
			
			g.forEachEdge(current, e -> {
				if( d[e.getStart()] != MAX_VALUE && d[e.getStart()] + e.getValue() < d[e.getEnd()] ) {
					d[e.getEnd()] = d[e.getStart()] + e.getValue();
					pi[e.getEnd()] = e.getStart();
				} 
			});
		}
		
		return new ShortestPathFromOneVertex(source, d, pi, steps);
	}

	public static ShortestPathFromOneVertex dijkstraWithHeap(Graph g, int source) {
		var nbVertices = g.numberOfVertices();
		var d = new int[nbVertices];
		var pi = new int[nbVertices];
		pi[source] = -1;

		Arrays.fill(d, MAX_VALUE);
		d[source] = 0;

		var steps = 0;
		var todo = new CustomBinaryHeap(g.numberOfVertices());
		todo.add(source, d[source]);

		while(!todo.isEmpty() ) {
			steps++;
			var current = todo.extractMin();

			g.forEachEdge(current, e -> {
				if( d[e.getStart()] != MAX_VALUE && d[e.getStart()] + e.getValue() < d[e.getEnd()] ) {
					d[e.getEnd()] = d[e.getStart()] + e.getValue();
					pi[e.getEnd()] = e.getStart();
					todo.add(e.getStart() , d[e.getStart()]);
					todo.add(e.getEnd() , d[e.getEnd()]);
				}
			});
		}

		return new ShortestPathFromOneVertex(source, d, pi, steps);
	}


}
